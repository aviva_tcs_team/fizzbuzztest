﻿namespace FizzBuzz.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;

    [TestClass]
    public class FizzBuzzUnitTest
    {
        private FizzBuzz fizzbuzz;
        private string expectedResult;

        /// <summary>
        /// Tests the initialize the fizzbuzz logic.
        /// </summary>
        [SetUp]
        public void TestInitialize()
        {
            this.fizzbuzz = new FizzBuzz();
        }

        [TestCase(15)]
        public void TestCheckNumberDivisibleBy3And5(int inputFizzBuzzNumber)
        {
            var actualResult = this.fizzbuzz.IsNumberDivisible(inputFizzBuzzNumber);
            actualResult.ShouldBeEquivalentTo(true);
        }

        [TestCase]
        public void TestGetFizzbuzzMessageStyle()
        {
            this.expectedResult = string.Empty;
            var actualResult = this.fizzbuzz.MessageStyle;
            actualResult.ShouldBeEquivalentTo(this.expectedResult);
        }
    }
}
