﻿namespace FizzBuzz.Business.Tests
{
    using System.Collections.Generic;
    using Moq;

    public class TestHelper
    {
        public List<IFizzBuzz> ExpectedResult;

        public TestHelper()
        {
            this.ExpectedResult = new List<IFizzBuzz>();
        }

        public void SetExpectedList()
        {
            this.ExpectedResult.Add(new DisplayNumber(1) { DisplayMessage = "1" });
            this.ExpectedResult.Add(new DisplayNumber(2) { DisplayMessage = "2" });
            this.ExpectedResult.Add(new Fizz() { DisplayMessage = "Fizz" });
            this.ExpectedResult.Add(new DisplayNumber(4) { DisplayMessage = "4" });
            this.ExpectedResult.Add(new Buzz() { DisplayMessage = "Buzz" });
            this.ExpectedResult.Add(new Fizz() { DisplayMessage = "Fizz" });
            this.ExpectedResult.Add(new DisplayNumber(7) { DisplayMessage = "7" });
            this.ExpectedResult.Add(new DisplayNumber(8) { DisplayMessage = "8" });
            this.ExpectedResult.Add(new Fizz() { DisplayMessage = "Fizz" });
            this.ExpectedResult.Add(new Buzz() { DisplayMessage = "Buzz" });
            this.ExpectedResult.Add(new DisplayNumber(11) { DisplayMessage = "11" });
            this.ExpectedResult.Add(new Fizz() { DisplayMessage = "Fizz" });
            this.ExpectedResult.Add(new DisplayNumber(13) { DisplayMessage = "13" });
            this.ExpectedResult.Add(new DisplayNumber(14) { DisplayMessage = "14" });
            this.ExpectedResult.Add(new FizzBuzz() { DisplayMessage = "Fizz Buzz" });
        }

        public List<IFizzBuzz> GetMockFizzBuzzList(int rowsToBeTested)
        {
            var mockList = new Mock<List<IFizzBuzz>>();
            this.SetExpectedList();
            for (var index = 0; index <= rowsToBeTested; index++)
            {
                var count = index + 1;
                if (this.ExpectedResult[index].IsNumberDivisible(count))
                {
                    this.ExpectedResult[index].SetDisplayMessage(false);
                    mockList.Object.Add(this.ExpectedResult[index]);
                }
                else
                {
                    mockList.Object.Add(new DisplayNumber(count));
                }
            }
            return mockList.Object;
        }
    }
}
