﻿namespace FizzBuzz.Web.Controllers
{
    using System.Diagnostics.CodeAnalysis;
    using System.Web.Mvc;
    using FizzBuzz.Business;
    using FizzBuzz.Web.Attributes;
    using FizzBuzz.Web.Mappers;
    using FizzBuzz.Web.Models;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzLogic fizzbuzzLogic;

        public FizzBuzzController(IFizzBuzzLogic fizzBuzzLogic)
        {
            this.fizzbuzzLogic = fizzBuzzLogic;
        }

        [HttpGet]
        public ActionResult FizzBuzzView()
        {
            return this.View("FizzBuzzView", new FizzBuzzModel());
        }

        [HttpPost]
        [Button(Name = "DisplayList")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzView(FizzBuzzModel fizzBuzzModel)
        {
            var resultSet = this.SetFizzBuzzModel(fizzBuzzModel, string.Empty);
            return this.View("FizzBuzzView", resultSet);
        }

        [HttpPost]
        [Button(Name = "Next")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewNext(FizzBuzzModel fizzBuzzModel)
        {
            var resultSet = this.SetFizzBuzzModel(fizzBuzzModel, "next");
            return this.View("FizzBuzzView", resultSet);
        }

        [HttpPost]
        [Button(Name = "Previous")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewPrevious(FizzBuzzModel fizzBuzzModel)
        {
            var resultSet = this.SetFizzBuzzModel(fizzBuzzModel, "previous");
            return this.View("FizzBuzzView", resultSet);
        }

        private FizzBuzzModel SetFizzBuzzModel(FizzBuzzModel fizzBuzzModel, string caseSwitchPaging)
        {
            var result = new FizzBuzzModel { FizzBuzzNumber = fizzBuzzModel.FizzBuzzNumber };
            FizzBuzzPagingMapper.SetFizzBuzzListPaging(result, caseSwitchPaging, this.TempData["FizzBuzzPage"]);
            if (this.ModelState.IsValid)
            {
                this.FillFizzBuzzResultSet(result);
            }

            this.TempData["FizzBuzzPage"] = result.fizzbuzzPaging;
            return result;
        }

        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1118:ParameterMustNotSpanMultipleLines", Justification = "Reviewed. Suppression is OK here.")]
        [NonAction]
        private void FillFizzBuzzResultSet(FizzBuzzModel fizzBuzzModel)
        {
            var startIndex = (fizzBuzzModel.fizzbuzzPaging.PageIndex - 1) * FizzBuzzModel.PageSize;
            if (fizzBuzzModel.FizzBuzzNumber == null)
            {
                return;
            }

            fizzBuzzModel.FizzBuzzNumberList = this.fizzbuzzLogic.GetFizzBuzzNumbersList(fizzBuzzModel.FizzBuzzNumber.Value);
            fizzBuzzModel.FizzBuzzNumberList = fizzBuzzModel.FizzBuzzNumberList.GetRange(
                startIndex,
                fizzBuzzModel.FizzBuzzNumber.Value - startIndex < FizzBuzzModel.PageSize
                    ? fizzBuzzModel.FizzBuzzNumber.Value - startIndex
                    : FizzBuzzModel.PageSize);
        }
    }
}