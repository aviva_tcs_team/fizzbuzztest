﻿namespace FizzBuzz.Web.Models
{
    public class FizzBuzzPaging
    {
        public FizzBuzzPaging()
        {
            this.PageIndex = 1;
        }

        public int PageIndex { get; set; }
        public int PageCount { get; set; }
    }
}