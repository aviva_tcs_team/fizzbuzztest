﻿namespace FizzBuzz.Business
{
    public class Fizz : IFizzBuzz
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return inputFizzBuzzNumber % 3 == 0;
        }

        public string MessageStyle
        {
            get { return "blue"; }
        }

        public string DisplayMessage { get; set; }

        public void SetDisplayMessage(bool isTodayWednesday)
        {
            this.DisplayMessage = isTodayWednesday ? "Wizz" : "Fizz";
        }
    }
}