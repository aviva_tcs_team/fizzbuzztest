﻿using System;

namespace FizzBuzz.Business
{
    public class DayProvider : IDayProvider
    {
        public bool IsTodayWednesday()
        {
            return DateTime.Today.DayOfWeek == DayOfWeek.Wednesday;
        }
    }
}
