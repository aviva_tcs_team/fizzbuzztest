﻿namespace FizzBuzz.Business
{
    using System.Collections.Generic;
    using System.Linq;

    public class FizzBuzzHelper : IFizzBuzzLogic
    {
        private readonly IEnumerable<IFizzBuzz> fizzBuzz;
        private readonly IDayProvider dayProvider;

        public FizzBuzzHelper(IEnumerable<IFizzBuzz> fizzBuzz, IDayProvider dayProvider)
        {
            this.fizzBuzz = fizzBuzz;
            this.dayProvider = dayProvider;
        }

        public List<IFizzBuzz> GetFizzBuzzNumbersList(int inputFizzBuzzNumber)
        {
            var fizzbuzzResultList = new List<IFizzBuzz>();
            for (var counter = 1; counter <= inputFizzBuzzNumber; counter++)
            {
                // Get the class instance if number is divisible by 3 or 5 or both
                var listFizzBuzzData = this.fizzBuzz.Where(x => x.IsNumberDivisible(counter));
                var fizzBuzzData = listFizzBuzzData as IList<IFizzBuzz> ?? listFizzBuzzData.ToList();
                if (fizzBuzzData.ToList().Count > 0)
                {
                    var fizzbuzzData = fizzBuzzData.ToList().LastOrDefault();
                    if (fizzbuzzData != null)
                    {
                        fizzbuzzData.SetDisplayMessage(this.dayProvider.IsTodayWednesday());
                        fizzbuzzResultList.Add(fizzbuzzData);
                    }
                }
                else
                {
                    fizzbuzzResultList.Add(new DisplayNumber(counter));
                }
            }

            return fizzbuzzResultList;
        }
    }
}