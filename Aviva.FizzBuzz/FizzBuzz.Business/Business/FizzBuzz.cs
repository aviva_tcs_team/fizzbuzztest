﻿namespace FizzBuzz.Business
{
    public class FizzBuzz : IFizzBuzz
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return (inputFizzBuzzNumber % 15) == 0;
        }

        public string MessageStyle
        {
            get { return string.Empty; }
        }

        public string DisplayMessage { get; set; }

        public void SetDisplayMessage(bool isTodayWednesday)
        {
            this.DisplayMessage = isTodayWednesday ? "Wizz Wuzz" : "Fizz Buzz";
        }
    }
}