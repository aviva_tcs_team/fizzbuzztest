﻿namespace FizzBuzz.Business
{
    using System;

    public class DisplayNumber : IFizzBuzz
    {
        public DisplayNumber(int inputFizzBuzzNumber)
        {
            this.DisplayMessage = Convert.ToString(inputFizzBuzzNumber);
        }

        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return false;
        }

        public string DisplayMessage { get; set; }

        public string MessageStyle
        {
            get { return string.Empty; }
        }

        public void SetDisplayMessage(bool isTodayWednesday)
        {
        }
    }
}