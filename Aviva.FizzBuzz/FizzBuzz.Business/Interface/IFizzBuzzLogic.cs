﻿namespace FizzBuzz.Business
{
    using System.Collections.Generic;

    public interface IFizzBuzzLogic
    {
        List<IFizzBuzz> GetFizzBuzzNumbersList(int inputFizzBuzzNumber);
    }
}