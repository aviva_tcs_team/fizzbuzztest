﻿namespace FizzBuzz.Business
{
    public interface IFizzBuzz
    {
        bool IsNumberDivisible(int number);
        string MessageStyle { get; }
        void SetDisplayMessage(bool isTodayWednesday);
        string DisplayMessage { get; set; }
    }
}