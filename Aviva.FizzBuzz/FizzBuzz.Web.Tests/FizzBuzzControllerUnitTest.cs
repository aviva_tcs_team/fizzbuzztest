﻿namespace FizzBuzz.Web.Tests
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Business;
    using FizzBuzz.Web.Controllers;
    using FizzBuzz.Web.Models;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using NUnit.Framework;

    [TestClass]
    public class FizzBuzzControllerUnitTest
    {
        private Mock<IFizzBuzzLogic> mockFizzBuzzLogic;
        private FizzBuzzController controller;

        [SetUp]
        public void TestInitialize()
        {
            this.mockFizzBuzzLogic = new Mock<IFizzBuzzLogic>();
            this.controller = new FizzBuzzController(this.mockFizzBuzzLogic.Object);
        }

        [TestCase("FizzBuzzView")]
        public void TestActionMethodReturnsFizzBuzzView(string expectedViewName)
        {                    
            var fizzbuzzController = new FizzBuzzController(null);
            var actualResult = fizzbuzzController.FizzBuzzView() as ViewResult;
            actualResult.Should().NotBeNull("Should return a ActionResult");
            actualResult.ViewName.ShouldBeEquivalentTo(expectedViewName);
        }

        [TestCase(20)]
        public void TestShouldDisplay20ItemsPerPage(int inputFizzBuzzNumber)
        {
            this.mockFizzBuzzLogic.Setup(m => m.GetFizzBuzzNumbersList(21)).Returns(this.GetFizzBuzzList(21));
            var result = this.controller.FizzBuzzView(new FizzBuzzModel { FizzBuzzNumber = 21 }) as ViewResult;
            if (result != null)
            {
                var viewModel = (FizzBuzzModel)result.Model;
                var actualResult = viewModel.FizzBuzzNumberList.Count;
                actualResult.ShouldBeEquivalentTo(inputFizzBuzzNumber);
            }
        }

        [TestCase(1)]
        public void TestShouldImplementPaging(int inputFizzBuzzNumber)
        {
            this.mockFizzBuzzLogic.Setup(m => m.GetFizzBuzzNumbersList(21)).Returns(this.GetFizzBuzzList(21));
            var result = this.controller.FizzBuzzViewNext(new FizzBuzzModel { fizzbuzzPaging = new FizzBuzzPaging() { PageIndex = 2 }, FizzBuzzNumber = 21 }) as ViewResult;
            if (result != null)
            {
                var viewModel = (FizzBuzzModel)result.Model;
                var actualResult = viewModel.FizzBuzzNumberList.Count;
                actualResult.ShouldBeEquivalentTo(inputFizzBuzzNumber);
            }
        }

        [TestCase(4)]
        public void TestShouldImplementPagecount(int inputFizzBuzzNumber)
        {
            this.mockFizzBuzzLogic.Setup(m => m.GetFizzBuzzNumbersList(63)).Returns(this.GetFizzBuzzList(63));
            var result = this.controller.FizzBuzzView(new FizzBuzzModel { FizzBuzzNumber = 63 }) as ViewResult;
            if (result != null)
            {
                var viewModel = (FizzBuzzModel)result.Model;
                var actualResult = viewModel.fizzbuzzPaging.PageCount;
                actualResult.ShouldBeEquivalentTo(inputFizzBuzzNumber);
            }
        }

        private List<IFizzBuzz> GetFizzBuzzList(int rowsToBeTested)
        {
            var fizzbuzzlist = new List<IFizzBuzz>();
            for (var index = 1; index <= rowsToBeTested; index++)
            {
                fizzbuzzlist.Add(new DisplayNumber(index));
            }

            return fizzbuzzlist;
        }
    }
}
